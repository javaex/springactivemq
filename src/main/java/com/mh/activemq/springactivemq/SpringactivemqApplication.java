package com.mh.activemq.springactivemq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringactivemqApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringactivemqApplication.class, args);
	}

}
